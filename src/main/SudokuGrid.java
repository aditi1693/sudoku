package main;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.*;

/**
 * This class holds and manages the main frame of the program.
 *
 **/
public class SudokuGrid {


    public JFrame frame;
    private boolean isStarted = false;

    private SudokuContent sudokuContent;

    private int size;
    private int tempSize;
    private String prevBoard[][];
    private String intialBoard[][];
    private String solvedBoard[][];
    private BackTracking backTracking;
    private long totalTimeTaken=0;
    private int onePossibleCount=0;
    private int backTrackingCount=0;
    private int humanKindCount=0;



    private final Timer timer = new Timer(1000, (ActionListener) null);
    final JLabel totalTime = new JLabel("Time Taken:");
    final JLabel backTrackingCountLabel = new JLabel("Backtracking Strategy Uses:");
    final JLabel onePossibleCountLabel = new JLabel("One-Possible Solution Uses:");
    final JLabel humanKindCountLabel = new JLabel("Human-Kind Strategy Uses:");
    final JButton fileChooser = new JButton("Choose File");
    final JButton startButton = new JButton("Start");
    final JButton solveButton = new JButton("Solve");
    final JTextField grid[][] = new JTextField[36][36];
    int timeCount = -1;

    public SudokuGrid(){
        sudokuContent = new SudokuContent("Input/Puzzle-4X4-0201.txt");

        String board[][] = sudokuContent.getBoard();
        this.backTracking=new BackTracking(board);
        this.intialBoard= backTracking.getSudokuBoard();
        long startTime = System.currentTimeMillis();
        OnePossibleSolution onePossibleSolution = new OnePossibleSolution(board);
        onePossibleSolution.solveSudoku();
        this.onePossibleCount = onePossibleSolution.getOnePossibleSolutionCount();
        board = onePossibleSolution.getGameBoard();
        HumanKind humanKind = new HumanKind(board);
        humanKind.solveSudoku();
        humanKind.display();
        this.humanKindCount = humanKind.getHumanKindCount();
        board = humanKind.getGameBoard();
        BackTracking backTracking = new BackTracking(board);
        backTracking.solveSudoku();
        this.backTrackingCount = backTracking.getBackTrackingCount();
        solvedBoard = backTracking.getSudokuBoard();
        backTracking.display();
        long endTime = System.currentTimeMillis();
        this.totalTimeTaken = (endTime - startTime);
        this.size = sudokuContent.getBoard_Size();
        tempSize = size;
        System.out.println("tempSize is "+tempSize);
        initialize(size, intialBoard, solvedBoard, totalTimeTaken,  onePossibleCount, humanKindCount, backTrackingCount);


    }

    public void initialize(int size, String intialBoard[][], String solvedBoard[][], long totalTimeTaken, int onePossibleCount,int humanKindCount, int backTrackingCount) {
        timer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                timeCount--;
                System.out.println(totalTimeTaken);
                totalTime.setText("Time Taken: " +totalTimeTaken+ " ms" );
                onePossibleCountLabel.setText("One-Possible Solution Uses:"+onePossibleCount );
                humanKindCountLabel.setText("Human-Kind Strategy Uses:"+humanKindCount );
                backTrackingCountLabel.setText("Backtracking Strategy Uses:"+backTrackingCount );
            }
        });


        this.frame = new JFrame();
        frame.setTitle("Sudoku Solver");

        if (size == 16) {
            frame.setBounds(100, 100, 1000, 750);
            frame.getContentPane().setBackground(Color.pink);
        }
        if (size == 9) {
            frame.setBounds(100, 100, 700, 450);
            frame.getContentPane().setBackground(Color.pink);

        }
        if (size == 4) {
            frame.setBounds(100, 100, 500, 250);
            frame.getContentPane().setBackground(Color.pink);

        }
        if (size == 25) {
            frame.setBounds(100, 100, 1600, 1050);
            frame.getContentPane().setBackground(Color.pink);

        }
        if (size == 36) {
            frame.setBounds(100, 100, 1800, 1350);
            frame.getContentPane().setBackground(Color.pink);

        }
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        //////////////////////////////////GRID START /////////////////////////////////////////////////////////////


        int h = 12, w = 13, hi = 39, wi = 37;
        Double sqrt = Math.sqrt(size);
        int squareRoot = sqrt.intValue();

        for (int i = 0; i < size; i++) {
            if (i % squareRoot == 0 && i != 0) w += 13;

            for (int j = 0; j < size; j++) {
                if (j % squareRoot == 0 && j != 0) h += 11;


                grid[i][j] = new JTextField();
                grid[i][j].setColumns(size + 1);
                grid[i][j].setBounds(h, w, 38, 37);
                frame.getContentPane().add(grid[i][j]);
                h += hi;
            }
            h = 12;
            w += wi;
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                grid[i][j].setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.BOLD, 22));
                grid[i][j].setHorizontalAlignment(SwingConstants.CENTER);
                grid[i][j].setEditable(false);
            }
        }
        //////////////////////////////////  GRID END  /////////////////////////////////////////////////////////////

        solveButton.setVisible(false);
        totalTime.setVisible(false);
        onePossibleCountLabel.setVisible(false);
        backTrackingCountLabel.setVisible(false);
        humanKindCountLabel.setVisible(false);

        solveButton.setFont(new Font("Calibri Light", Font.BOLD, 18));
        if (size == 36) {
            solveButton.setBounds(1400, 556, 155, 37);
        }
        if (size == 25) {
            solveButton.setBounds(1120, 456, 155, 37);
        }
        if (size == 16) {
            solveButton.setBounds(700, 306, 155, 37);
        }
        if (size == 9) {
            solveButton.setBounds(420, 206, 155, 37);
        }
        if (size == 4) {
            solveButton.setBounds(220, 86, 155, 37);

        }
        frame.getContentPane().add(solveButton);


        totalTime.setFont(new Font("Segoe UI Black", Font.BOLD, 13));
        totalTime.setFont(new Font("Calibri Light", Font.BOLD, 16));
        if (size == 36) {
            totalTime.setBounds(1340, 13, 176, 16);
        }
        if (size == 25) {
            totalTime.setBounds(1100, 13, 176, 16);
        }
        if (size == 16) {
            totalTime.setBounds(670, 13, 176, 16);
        }
        if (size == 9) {
            totalTime.setBounds(390, 13, 176, 16);
        }
        if (size == 4) {
            totalTime.setBounds(200, 13, 176, 16);

        }

        frame.getContentPane().add(totalTime);

        onePossibleCountLabel.setFont(new Font("Segoe UI Black", Font.BOLD, 13));
        onePossibleCountLabel.setFont(new Font("Calibri Light", Font.BOLD, 16));
        if (size == 36) {
            onePossibleCountLabel.setBounds(1340, 33, 220, 16);
        }
        if (size == 25) {
            onePossibleCountLabel.setBounds(1100, 33, 220, 16);
        }
        if (size == 16) {
            onePossibleCountLabel.setBounds(670, 33, 220, 16);
        }
        if (size == 9) {
            onePossibleCountLabel.setBounds(390, 33, 220, 16);
        }
        if (size == 4) {
            onePossibleCountLabel.setBounds(200, 33, 220, 16);

        }

        frame.getContentPane().add(onePossibleCountLabel);

        humanKindCountLabel.setFont(new Font("Segoe UI Black", Font.BOLD, 13));
        humanKindCountLabel.setFont(new Font("Calibri Light", Font.BOLD, 16));
        if (size == 36) {
            humanKindCountLabel.setBounds(1340, 53, 220, 16);
        }
        if (size == 25) {
            humanKindCountLabel.setBounds(1100, 53, 220, 16);
        }
        if (size == 16) {
            humanKindCountLabel.setBounds(670, 53, 220, 16);
        }
        if (size == 9) {
            humanKindCountLabel.setBounds(390, 53, 220, 16);
        }
        if (size == 4) {
            humanKindCountLabel.setBounds(200, 53, 220, 16);

        }

        frame.getContentPane().add(humanKindCountLabel);

        backTrackingCountLabel.setFont(new Font("Segoe UI Black", Font.BOLD, 13));
        humanKindCountLabel.setFont(new Font("Calibri Light", Font.BOLD, 16));
        if (size == 36) {
            backTrackingCountLabel.setBounds(1340, 73, 220, 16);
        }
        if (size == 25) {
            backTrackingCountLabel.setBounds(1100, 73, 220, 16);
        }
        if (size == 16) {
            backTrackingCountLabel.setBounds(670, 73, 220, 16);
        }
        if (size == 9) {
            backTrackingCountLabel.setBounds(390, 73, 220, 16);
        }
        if (size == 4) {
            backTrackingCountLabel.setBounds(200, 73, 220, 16);

        }

        frame.getContentPane().add(backTrackingCountLabel);

        if (size == 36) {
            startButton.setBounds(1400, 556, 155, 37);
        }
        if (size == 25) {
            startButton.setBounds(1120, 456, 155, 37);
        }
        if (size == 16) {
            startButton.setBounds(700, 306, 155, 37);
        }
        if (size == 9) {
            startButton.setBounds(420, 206, 155, 37);
        }
        if (size == 4) {
            startButton.setBounds(220, 86, 155, 37);

        }
        frame.getContentPane().add(startButton);
        startButton.setFont(new Font("Calibri Light", Font.BOLD, 18));
        startButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (isStarted) {
                    isStarted = false;

                    for (int i = 0; i < size; i++) {
                        for (int j = 0; j < size; j++) {
                            grid[i][j].setEditable(false);
                            grid[i][j].setText(intialBoard[i][j]);
                        }
                    }
                    startButton.setText("Start");
                    timer.stop();
                    totalTime.setVisible(false);
                    solveButton.setVisible(false);
                    onePossibleCountLabel.setVisible(false);
                    backTrackingCountLabel.setVisible(false);
                    humanKindCountLabel.setVisible(false);
                } else {

                    String board[][] = new String[size][size];
                    for (int i = 0; i < size; i++) {
                        for (int j = 0; j < size; j++) {
                            board[i][j] = intialBoard[i][j];
                        }
                    }
                    System.out.println("Size" + size);
                    for (int i = 0; i < size; i++) {
                        for (int j = 0; j < size; j++) {

//                            prevBoard[i][j] = board[i][j];
                        }
                    }
                    for (int i = 0; i < size; i++) {
                        for (int j = 0; j < size; j++) {
                            if (!board[i][j].equals("0")) {
                                grid[i][j].setText(board[i][j]);
                                grid[i][j].setBackground(Color.lightGray);
                            } else {
                                grid[i][j].setText(" ");
                            }
                        }
                    }

                    solveButton.setVisible(true);
                    startButton.setVisible(false);
                    totalTime.setVisible(false);
                    onePossibleCountLabel.setVisible(false);
                    backTrackingCountLabel.setVisible(false);
                    humanKindCountLabel.setVisible(false);
                    timer.start();
                    isStarted = true;
                }
            }
        });

        solveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                if(!isStarted){
                    isStarted = false;
                    for(int i = 0; i < size; i++){
                        for(int j = 0; j < size; j++){
                            grid[i][j].setEditable(false);
                            if(!intialBoard[i][j].equals(solvedBoard[i][j]))
                            {
                                grid[i][j].setText(solvedBoard[i][j]);


                            }

                            grid[i][j].setText(solvedBoard[i][j]);

                        }
                    }


                    totalTime.setVisible(true);
                    onePossibleCountLabel.setVisible(true);
                    backTrackingCountLabel.setVisible(true);
                    humanKindCountLabel.setVisible(true);
                    solveButton.setVisible(false);
                }
                else{
                    for(int i = 0; i < size; i++){
                        for(int j = 0; j < size; j++){
                            grid[i][j].setText(solvedBoard[i][j]);



                        }
                    }
                    solveButton.setVisible(false);
                    startButton.setVisible(false);
                    totalTime.setVisible(true);
                    onePossibleCountLabel.setVisible(true);
                    backTrackingCountLabel.setVisible(true);
                    humanKindCountLabel.setVisible(true);
                    timer.start();
                    isStarted = true;
                }
            }


        });
    }


}