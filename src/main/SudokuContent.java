package main;

import java.io.*;

public class SudokuContent {

    private String[][] array;
    private String[][] board;
    private int board_Size;
    private String sudokuNumbers[];

    public SudokuContent(String fileName) {


        try {
            FileReader in = new FileReader(fileName);
            BufferedReader bf = new BufferedReader(in);
            this.board_Size = Integer.valueOf(bf.readLine()); //get the number of lines in the file

            array = new String[board_Size + 2][board_Size];
            this.board = new String[board_Size+2][board_Size];
            int j = 0;
            String text;
            for (int i = 0; i < board_Size+1; i++)   {
                String allValues = bf.readLine();
                //ystem.out.println(allValues);
                String temp[] = allValues.split(" ");

                for (int l = 0; l < temp.length; l++) {
                    if (temp[l].equals("-")) {
                        array[j][l] = "0";
                    } else {
                        array[j][l] = temp[l];
                        //    System.out.println(array[j][l]);
                    }
                }
                j++;
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        // line is not visible here.
        sudokuNumbers=new String[board_Size];
        for(int a=0; a< array[0].length; a++){

            this.sudokuNumbers[a]= array[0][a];
        }



        for (int m = 0; m < board_Size + 1; m++) {
            for (int n = 0; n < board_Size; n++) {
                this.board[m ][n] = array[m][n];
            }
        }

    }

    public int getBoard_Size() {
        return board_Size;
    }

    public String[][] getBoard() {
        return board;
    }

    public  void writeFile(File fout,String [][]gameBoard, int BTCount, int OPCount, int HKCount, long timeTaken, long time1, long time2, long time3) throws IOException {

        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        bw.write(String.valueOf(gameBoard[0].length));
        bw.newLine();
        for (int i = 0; i < this.board_Size + 1; i++) {
            for (int j = 0; j < this.board_Size; j++) {
                if (this.array[i][j].equals("0")) {
                    bw.write("-");
                    bw.write(" ");
                } else {
                    bw.write(this.array[i][j]);
                    bw.write(" ");
                }
            }
            bw.newLine();

        }

        bw.newLine();
        bw.newLine();
        bw.write("Solution:");
        bw.newLine();
        bw.newLine();

        for (int i = 1; i < this.board_Size + 1; i++) {
            for (int j = 0; j < this.board_Size; j++) {
                bw.write(gameBoard[i][j]);
                bw.write(" ");
            }
            bw.newLine();
        }

        bw.newLine();
        bw.newLine();

        bw.write("Total Time:   ");
        bw.write(String.valueOf(timeTaken)+"   ns");
        bw.newLine();
        bw.newLine();
        bw.newLine();
        bw.write("Strategy               ");
        bw.write("Uses          ");
        bw.write("Time   ");
        bw.newLine();
        bw.write("One Possible Solution  ");
        bw.write(OPCount+"      ");
        bw.write(String.valueOf(time1)+"  ns");
        bw.newLine();
        bw.write("Human Kind Strategy    ");
        bw.write(HKCount+"      ");
        bw.write(String.valueOf(time2)+"  ns");
        bw.newLine();
        bw.write("BackTracking           ");
        bw.write(BTCount+"      ");
        bw.write(String.valueOf(time3)+"  ns");
        bw.close();
    }

    public boolean isValidSudoku(String[][] board, int size) {
        String sudokuBoard[][]= new String[size][size];
        for (int i=1; i<size+1; i++){
            for(int j=0;j<size;j++){
                sudokuBoard[i-1][j]=board[i][j];
            }
        }
        if (sudokuBoard == null ) {
            System.out.println("Sudoku is null");
            return false;
        }
        for (int i=1; i<size; i++)
        {
            if( sudokuBoard[i].length != size){
                System.out.println("Invalid Format");
                return false;
            }}
        for (int i=1; i<size; i++)
        {
            if( sudokuBoard.length != size){
                System.out.println("Invalid Format");
                return false;
            }
        }
        //Check Each Column
        for (int i = 0; i < size; i++) {
            String[] colBoard = new String[size];
            for (int j = 0; j < size; j++) {
                colBoard[j] = sudokuBoard[j][i];
                //System.out.println("values in colBoard are : "+colBoard[j].getVal());
            }
            int m = 0;
            while (m < colBoard.length) {
                int n = 0;
                while (n < colBoard.length) {
                    //  System.out.println("value of m and n are : "+colBoard[m].getVal()+","+colBoard[n].getVal());
                    if (colBoard[m].equals(colBoard[n])) {
                        if (m == n) {
                            //System.out.println("value of m and n are equal");
                        } else {
                            if (Integer.parseInt(colBoard[m]) == 0 && Integer.parseInt(colBoard[m]) == 0) {
                                //          System.out.println("value of m and n are zero");
                            } else {
                                System.out.println("Invalid Format");
                                return false;
                            }
                        }
                    }
                    n++;
                }
                m++;
            }
        }
        //Check Each Row
        for (int i = 0; i < size; i++) {
            String[] rowBoard = new String[size];
            for (int j = 0; j < size; j++) {
                rowBoard[j] = sudokuBoard[i][j];
                //  System.out.println("values in rowBoard are : "+rowBoard[j].getVal());
            }
            int m = 0;
            while (m < rowBoard.length) {
                int n = 0;
                while (n < rowBoard.length) {
                    //    System.out.println("value of m and n are : "+rowBoard[m].getVal()+","+rowBoard[n].getVal());
                    if (rowBoard[m].equals(rowBoard[n])) {
                        if (m == n) {
                            //System.out.println("value of m and n are equal");
                        } else {
                            if (Integer.parseInt(rowBoard[m]) == 0 && Integer.parseInt(rowBoard[m]) == 0) {
                                //              System.out.println("value of m and n are zero");
                            } else {
                                System.out.println("Invalid Format");
                                return false;
                            }
                        }
                    }
                    n++;
                }
                m++;
            }
        }
        // check that num is not repeated in subgrid
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                String[] subGrid = getSubGrid(i, j, String.valueOf(sudokuBoard[i][j]), size, sudokuBoard);
                for (int p = 0; p < subGrid.length; p++) {
                    //System.out.println("subGrid values are : "+subGrid[p].getVal());
                }
                int m = 0;
                while (m < subGrid.length) {
                    int n = 0;
                    while (n < subGrid.length) {
                        //  System.out.println("value of m and n are : "+subGrid[m].getVal()+","+subGrid[n].getVal());
                        if (subGrid[m].equals(subGrid[n])) {
                            if (m == n) {
                                //System.out.println("value of m and n are equal");
                            } else {
                                if (Integer.parseInt(subGrid[m]) == 0 && Integer.parseInt(subGrid[m]) == 0) {
                                    //            System.out.println("value of m and n are zero");
                                } else {
                                    System.out.println("Invalid Format");
                                    return false;
                                }
                            }
                        }
                        n++;
                    }
                    m++;
                }
            }
        }
        return true;
    }
    //to get a particular row of sudoku
    public String[] getSubGrid(int row, int column, String number, int size, String board[][]){
        String [] subGridValues = new String[size];
        Double sqrt = Math.sqrt(size);
        int squareRoot = sqrt.intValue();
        int r = row - row%squareRoot;
        int c = column - column%squareRoot;
        int k = 0;
        for(int i = r; i<r+squareRoot; i++){

            for(int j = c; j<c+squareRoot; j++){
                subGridValues[k] = board[i][j];
                k++;
            }
        }

        return subGridValues;
    }

}
