package test;

import static org.junit.Assert.assertEquals;

import main.BackTracking;

import main.HumanKind;
import main.OnePossibleSolution;
import main.SudokuContent;
import org.junit.runner.RunWith;
import org.junit.Test;

import static org.junit.Assert.*;

import static org.junit.Assert.*;


public class OnePossibleSolutionTest {
    @Test
    public void onepossibleSolutionSolve(){
        SudokuContent sudokuContent = new SudokuContent("input\\Puzzle-9x9-0101.txt");
        String board[][]=sudokuContent.getBoard();
         OnePossibleSolution onePossibleSolution= new OnePossibleSolution(board);
        assertEquals(onePossibleSolution.getSize(),board[0].length);
        onePossibleSolution.solveSudoku();
        assertEquals(2,onePossibleSolution.getOnePossibleSolutionCount());
    }

}