package test;

import main.BackTracking;

import main.HumanKind;
import main.SudokuContent;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.Test;

import static org.junit.Assert.*;
public class HumanKindTest {

    @Test
    public void humanKindTestSolve(){
        SudokuContent sudokuContent = new SudokuContent("input\\Puzzle-9x9-0101.txt");
        String board[][]=sudokuContent.getBoard();
        HumanKind humanKind= new HumanKind(board);
        assertEquals(humanKind.getSize(),board[0].length);
        humanKind.solveSudoku();
        assertEquals(humanKind.getHumanKindCount(),1);
    }

}