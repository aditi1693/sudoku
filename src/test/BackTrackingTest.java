package test;

import main.BackTracking;

import main.SudokuContent;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.Test;

import static org.junit.Assert.*;


public class BackTrackingTest {
    @Test
    public void  testBackTracking() {
        SudokuContent sudokuContent = new SudokuContent("input\\Puzzle-9x9-0101.txt");
        String board[][]=sudokuContent.getBoard();
        BackTracking backTracking= new BackTracking(board);
        assertEquals(backTracking.getSize(),board[0].length);
        backTracking.solveSudoku();
        assertEquals(backTracking.getBackTrackingCount(),206);


    }



}
