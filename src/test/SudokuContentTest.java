package test;
import main.BackTracking;
import main.SudokuContent;

import java.io.File;
import java.io.IOException;
import org.junit.Test;

import static org.junit.Assert.*;

import static junit.framework.TestCase.assertTrue;

public class SudokuContentTest {
    @Test
    public void sudokuContentConstructor() throws IOException {
        SudokuContent sudokuContent = new SudokuContent("input\\Puzzle-9x9-0101.txt");
        String board[][]=sudokuContent.getBoard();
        int backTrackingCount=0;
        int onePossibleCount=0;
        int humanKindCount=0;
        long totalTimeTaken1=0;
        long totalTimeTaken2=0;
        BackTracking backTracking=new BackTracking(board);
        assertTrue(sudokuContent.isValidSudoku(board, board[0].length));
        long startTime = System.nanoTime();


        long startTime3 = System.nanoTime();
        backTracking.solveSudoku();
        long endTime3 = System.nanoTime();
        long totalTimeTaken3=endTime3-startTime3;
        backTrackingCount=backTracking.getBackTrackingCount();
        board = backTracking.getGameBoard();
        backTracking.display();
        long endTime = System.nanoTime();
        long totalTimeTaken=endTime-startTime;
        File fout = new File("out.txt");
        sudokuContent.writeFile(fout,board,onePossibleCount,humanKindCount,backTrackingCount,totalTimeTaken,totalTimeTaken1,totalTimeTaken2,totalTimeTaken3);

    }

}